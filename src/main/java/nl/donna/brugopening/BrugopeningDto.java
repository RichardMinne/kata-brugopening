package nl.donna.brugopening;

import java.util.Date;

/**
 * Er zijn diverse spoorbruggen in Nederland die open en dicht kunnen. Voor deze
 * bruggen worden brugopeningen gepland in Donna.
 */
public class BrugopeningDto {

    private Long id;

    private String brugnaam;

    private Date tijdOpen;

    private Date tijdDicht;

    // ///////////////// Getters en setters /////////////////////////

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrugnaam() {
        return brugnaam;
    }

    public void setBrugnaam(String brugnaam) {
        this.brugnaam = brugnaam;
    }

    public Date getTijdOpen() {
        return tijdOpen;
    }

    public void setTijdOpen(Date tijdOpen) {
        this.tijdOpen = tijdOpen;
    }

    public Date getTijdDicht() {
        return tijdDicht;
    }

    public void setTijdDicht(Date tijdDicht) {
        this.tijdDicht = tijdDicht;
    }

    @Override
    public String toString() {
        return "BrugopeningDto{" +
                "id='" + id + '\'' +
                ", brugnaam='" + brugnaam + '\'' +
                ", tijdOpen=" + tijdOpen +
                ", tijdDicht=" + tijdDicht +
                '}';
    }
}
