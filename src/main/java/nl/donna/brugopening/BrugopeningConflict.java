package nl.donna.brugopening;

/**
 * Dit object wordt gemaakt wanneer er een conflict is tussen 2 brugopeningen.
 */
public class BrugopeningConflict {

	private String melding;

	private BrugopeningDto brugopening1;

	private BrugopeningDto brugopening2;

	// ///////////////// Getters en setters /////////////////////////
	public String getMelding() {
		return melding;
	}

	public void setMelding(String melding) {
		this.melding = melding;
	}

	public BrugopeningDto getBrugopening1() {
		return brugopening1;
	}

	public void setBrugopening1(BrugopeningDto brugopening1) {
		this.brugopening1 = brugopening1;
	}

	public BrugopeningDto getBrugopening2() {
		return brugopening2;
	}

	public void setBrugopening2(BrugopeningDto brugopening2) {
		this.brugopening2 = brugopening2;
	}

}
