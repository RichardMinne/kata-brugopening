# Brugopening kata

Er zijn diverse spoorbruggen in Nederland die open en dicht kunnen. Voor deze
bruggen worden brugopeningen gepland in Donna.

* Brugopening kata voor intake DSO.
* Tijdsduur: 45 minuten
  

## Opdracht programmeurs

1. Er is een brugopening conflict wanneer er voor één brug twee brugopeningen tegelijkertijd
  gepland zijn. Schrijf een functie die een lijst met brugopeningen controleert op conflicten.
2. Schrijf een test voor opdracht1
3. Stel dat de lijst met brugopeningen erg groot kan worden. Zou je dan de oplossing uit opdracht1
   hier nog op aanpassen ?

* * * 

## Opdracht testers

1. Er is een brugopening conflict wanneer er voor één brug twee brugopeningen tegelijkertijd gepland zijn.
   Schrijf een functie die een lijst met brugopeningen controleert op conflicten.
   Los de case op in pseudo code of in Java code.
   Bedenk in ieder geval het algoritme, en de signatuur van de gevraagde functie.
   Geef zo mogelijk de implementatie daarvan in termen van benodigde interfaces/klassen (incl. relaties),
   member variabelen en methodes (incl. signatuur).
   Je hoeft de methodes niet in detail uit te werken, als maar duidelijk is wat ze doen.

2. In de applicatie kunnen brugopeningen worden opgevoerd.
   Wat voor controles zou je daarbij willen uitvoeren?
   Beschrijf kort je aanpak voor een functionele test daarvan.
   Wat voor testtechnieken zou je gebruiken?

3. Schrijf enkele Gherkin (Given/When/Then) scenario's voor een automatische test voor brugopening conflicten.  
   Beschrijf hoe je de testdata zou willen opzetten.
   Geef aan of de verschillende scenario's onafhankelijk van elkaar zijn of op elkaar voortborduren,
   m.a.w. of de testdata al of niet bij elk testgeval opnieuw wordt opgezet en weer ontmanteld.
   N.B. Je hoeft geen implementatie van de Given/When/Then stappen te maken in java of pseudo-code,
   het gaat alleen maar om de scenario's zelf.
